# General H1 states file for sensor correction

# Would be nice if we could get this to work for both BSCs and HAMs

from guardian import GuardState, GuardStateDecorator
from isiguardianlib.sensor_correction.states import *
from isiguardianlib.sensor_correction.fader import Fader
from isiguardianlib.sensor_correction import edges as sc_edges
from . import const


#############################################
# States

class INIT(GuardState):
    """Check the current configuration and then go to that state.
    If it is in an unknown state, we need to figure out where we 
    should go.
    
    In the mean time maybe we should have this state just go to down?
    """
    def main(self):
        for config, dof_dict in const.SC_FM_confs.items():
            if check_config_filter_engaged(dof_dict):
                return 'CONFIG_{}'.format(config)
        log('In unknown state, parking in DOWN')
        return 'DOWN'

        
# A parking spot for us to run tests, or to go after INIT when unknown
class DOWN(GuardState):
    index = 5
    def run(self):
        return True
    

# Autogenerate the configuration states and edges
make_config_states(const.SC_FM_confs)
edges = sc_edges.make_gen_edges(const.SC_FM_confs)
